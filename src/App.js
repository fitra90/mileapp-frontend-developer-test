import React from "react";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import logo from "./assets/logo.png";
import illustrator from "./assets/management.png";
function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {"Copyright © 2019 PT. Paket Informasi Digital. "}
    </Typography>
  );
}

const useStyles = makeStyles((theme) => ({
  root: {
    // display: "flex",
    // justifyContent: "center",
    // alignItems: "center",
    // marginLeft: 520,
    left: "50%",
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary,
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

export default function App() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Grid
        container
        spacing={3}
        direction="column"
        alignItems="center"
        justify="center"
        style={{ minHeight: "100vh" }}
      >
        <img
          src={illustrator}
          alt="illustrator"
          style={{ position: "absolute", width: "500px", left: "62%" }}
        />
        <Grid item xs={3} style={{ position: "absolute" }}>
          <Paper className={classes.paper}>
            <CssBaseline />
            <div className={classes.paper}>
              <img
                src={logo}
                alt="logo"
                style={{ marginBottom: 20, width: "250px" }}
              />
              <Typography style={{ fontSize: 14 }}>
                Platform Anda untuk mengelola semua manajemen layanan di
                lapangan
              </Typography>
              <form className={classes.form} noValidate>
                <TextField
                  variant="outlined"
                  margin="normal"
                  required
                  fullWidth
                  id="organisasi"
                  label="Nama organisasi Anda"
                  name="organisasi"
                  autoFocus
                />
                <Button
                  type="submit"
                  fullWidth
                  variant="contained"
                  color="primary"
                  className={classes.submit}
                >
                  Masuk
                </Button>
              </form>
            </div>
          </Paper>

          <Box mt={8}>
            <Copyright />
          </Box>
        </Grid>
      </Grid>
    </div>
  );
}
